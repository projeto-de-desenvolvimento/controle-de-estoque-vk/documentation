INSERT INTO `tutorials`(`sys_id`, `title`, `position`, `description`, `content`, `class_id`) 
VALUES ('SYS', 'Algoritmos', 1, 'Conceituação de um algoritmo, programa, problema computacional', 'Algoritmo é a base da ciência da computação e da programação. Você já pode ter ouvido este termo antes mas ficou em dúvida sobre seu significado. Algumas pessoas confundem o termo **algoritmo** com o termo **logarítmo**, mas geralmente essas pessoas não são da área da computação 😉 .

Na verdade o conceito de algoritmo é bastante simples, e você já deve ter realizado um algoritmo não-computacional muitas vezes na sua vida. Quando você levanta pela manhã 😴 , quando você sai de casa ☔ , quando almoça 😋 .Você sempre executando tarefas enquanto realiza análises de decisões, análises de possibilidades, valida argumentos e diversos outros processos. Esses são o que chamamos de **algoritmos não estruturados**. Um **manual de instruções** ou uma **receita de bolo** são exemplos de algoritmos não estruturados, mas vamos nos focar no caso dos **algoritmos estruturados**, que são a base da escrita de um programa de computador

Todo programa de um computador é montado por algoritmos que resolvem problemas matemáticos lógicos com objetivos específicos. Mesmo pessoas que usam uma **linguagem de programação** para fazer seus programas de computadores estão, na realidade, elaborando algoritmos computacionais em suas mentes 🤔💭 . Se você detalhar bastante um algoritmo para resolução de um **problema**, passar este algoritmo para uma linguagem de programação será moleza. Aliás há uma linguagem de programação que abstrai bastante de detalhes de uma linguagem de uso profissional como por exemplo **Javascript**, e permite uma tradução de 1 para 1 do seu algoritmo para uma linguagem computacional. Essa linguagem foi desenvolvida no Brasil 👏 e tem o nome de **PORTUGOL** (uma mistura de Português e mais outras duas linguagens: ALGOL e PASCAL). Se o inglês não for o seu forte experimente dar uma brincada com PORTUGOL e construir pequenos programas.
Mas vamos voltar ao conceito de algoritmo. Há uma sequência de passos usada por programadores por muito tempo para a escrita de um algoritmo computacional:
1. Preste atenção à ordem lógica da execução das tarefas;
1. Lembre-se de que ele deve ter um **início** e **fim**;
1. Ele deve ser completo (deve descrever como resolver o problema de forma total);
1. Deve ter um alto nível de detalhes;
1. Cada tarefa é uma instrução, assim, defina-as bem.

Você deve ter notado 3 coisas importantes nessa lista de passos. Um algoritmo deve ter fim, ou seja, ser uma **sequência de instruções finita**; Ele deve resolver um **problema específico**; E cada tarefa tem um relação de 1 para 1 com uma instrução (você verá mais sobre o conceito de instruções em breve, por favor não chame instruções de comandos :)

Resumindo tudo:
- Um algoritmo é uma sequência de instruções finita que busca resolver um problema específico.
- Um problema específico pode ser a solução de um problema matemático como usar a fórmula de Pitágoras 🧮 , ou realizar a criptografia de uma palavra  🔑 . Um algoritmo não pode resolver um problema abstrato ou filosófico, como achar o sentido da vida (embora os especialistas em Inteligência Artificial 🤓 irão dizer o contrário ).
- Um programa de computador é um software. Ele pode ser bem simples e consistir de apenas um algoritmo, ou bem complexo  como um programa de edição de vídeo que exige a programação de muitos algoritmos escritos em uma linguagem de programação de computador.'
, 1);