Você já viu como declarar uma variável com a palavra **let**, e sabe que pode modificar o seu valor a qualquer momento 🧠 . Por exemplo:\
`let melhorJogoCriado = "Zelda"`\
`melhorJogoCriado = "GTA V"`
Você viu até que é possível, embora não aconselhável mudar o tipo de uma variável. Como:\
`let numero = 1`\
`numero = "Um texto qualquer"` (repare nas aspas)

Mas digamos que em seu programa ou jogo, certas medidas ou quantidades serão **constantes**, por exemplo o seu programa desenha um círculo e para isso você vai precisar do valor de pi. Você está com preguiça de pegar uma calculadora (e vamos fingir que nem no seu celular, nem no seu computador existe uma calculadora 😒 ) e se lembra que pi é em torno de 3. Então você declara\
`const PI = 3`\
E utiliza essa constante por todo o programa quando precisar. Você irá notar, neste exemplo hipotético, que os seus círculos ficaram estranhos. Isto é porque utilizamos um valor aproximado de pi. "E agora !!! Meu programa não presta para nada 🤬 ": o seu eu hipotético estará gritando. Calma, há uma solução fácil para isso. Vá lá na declaração de pi, no início do programa e mude a **atribuição de valor** de pi. Mude a linha\
`const PI = 3`\
para\
`const PI = 3.1416`\
E agora todos os lugares no seu programa que usam essa **constante** estarão com esse novo valor, e seu círculo estará bem redondinho. Agora você pode estar se perguntando "Eu poderia fazer a mesma coisa com let", e isto é verdade; mas há uma vantagem que o uso de const dá a você como programador: você não pode mudá-las após a sua declaração. Neste exemplo o que mudamos foi o **valor na declaração** e não o **valor após a declaração**, e isso faz muita diferença. Tente modificar o valor de `PI` para 5.\
`PI = 5`\
E espero que o erro `Assignment to constant variable.` tenha aparecido no seu console do browser. O uso de constantes impede que você ou outro programador que esteja trabalhando no mesmo programa mude o valor de uma variável que deveria ser imutável. Geralmente são textos, booleans, quantidades, parâmetros que permitem que o programa funcione; mas que não se deseja que sejam alterados, a não ser em situações específicas como foi o caso do programa do círculo.
Resumindo:

- Use **const** quando você for utilizar um mesmo valor em vários lugares em seu programa e ele não for alterado pela lógica de seu programa.
- Use **const** para a proteção do valor em suas variáveis (eu sei que essa frase é paradoxal, mas não há maneira melhor de se referir a endereços nomeados na memória do que variáveis).

Á 😮 ! e mais um detalhe que você deve ter percebido: as constantes são nomeadas com **todas as letras em maiúsculas**. Você pode nomeá-las de outra forma, mas é uma ótima prática de programação nomear de acordo com essa regra. E depois ficará mais fácil achar as suas constantes quando os seus programas e jogos ficarem gigantes 👩‍💻 ▶🔟🔟🔟🔟 .
