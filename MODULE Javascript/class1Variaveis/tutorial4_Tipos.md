Você está no caminho certo para se tornar um programador JS bem sucedido. Mas antes de se aventurar por condicionais e operadores você precisa dominar o segredo dos **tipos** em JS 🕵 . Na verdade não há nenhum segredo e eles estão bem documentados na página de publicação das especificações do **ECMAScript** ( nome oficial por o qual nosso querido JavaScript é chamado). Mas a documentação oficial é bem maçante, e acredito que tenha chamado a sua atenção com a história de segredo 😒 .

Só um momento! Você deve estar pensando 🤔 . JS não é uma linguagem **fracamente tipada**? Porque eu devo saber sobre tipos? Isso vai fazer diferença na minha vida como programador JS? E a resposta infelizmente, ou felizmente dependendo do prisma 🔺🌈 que se observa é **SIM**, você precisa saber sobre tipos em JS. Por mais que ela seja uma linguagem fracamente tipada, **internamente** existem tipos e como você viu anteriormente lá no primeiro tutorial 🧠 , a palavra-chave `typeof` permite averiguar o tipo de uma variável. A verdade é que JS é da família de linguagens chamadas **de tipo dinâmico** ou **dinamicamente tipadas**, assim como Python. Mas vamos seguir e ver quais são esses tipos:

# Number

Já vimos esse tipo antes, e ele é um número 😒 . Até aí nenhuma surpresa. Ele pode ser um número **inteiro** ou **fracionário**. Você pode declará-los assim:\
`let num = 123`\
`let pi = 3.1416`

Agora temos um caso interessante se você tentar dividir um número positivo por zero. Tente no console do browser:\
`alert(1/0)`\
E espero que o resultado tenha sido `Infinity`, que é um número infinito positivo (aqueles que estudaram Cálculo entenderão quando se diz que um número tende ao infinto positivo 🤓 ). Agora faça a mesma coisa, mas com -1 ao invés de 1. `console.log(-1/0)`. Temos `-Infinity` que seria um número infinito negativo 🤯 .

E por fim temos o `NaN` de **_Not a Number_** ou **_Não é um Número_**, que curiosamente é do tipo Número (um pouco paradoxal se você parar para pensar 🤔 . Talvez os criadores do JS não quiseram criar mais um tipo só para esse caso específico). Ele acontece em situações especiais, quando você tenta utilizar um número com um outro tipo de variável com um operador (mais sobre operadores em breve ⏳ ) que não faz sentido para esses dois tipos.
Como por exemplo:\
`console.log("Isso é um texto. E textos não são divisíveis por números"/2)`\
Isso ocorre porque o operador `/` não sabe o que fazer quando encontra um texto e um número, e a melhor resposta que ele pode dar a você é que "Não, isto não é um número !", isto é um `NaN`.

# String

Também já vimos esse tipo antes 🧠 . Quando colocamos qualquer coisa entre aspas duplas ou simples vocês está criando uma String em JS. Mesmo que você não a salve em uma variável, o interpretado de JS cria por baixo dos panos, sorrateiramente 😼 uma **variável de ambiente** com qualquer valor que você tenha colocado entre aspas (duplas ou simples). Então `console.log("Esse é um texto qualquer")` está criando uma **variável de ambiente** com o conteúdo `Esse é um texto qualquer` do tipo **string**, mesmo que você não tenha declarado uma variável para isso. Você vai ver um conceito que se aproxima de variáveis de ambiente na medida em que são imutáveis logo em breve ⏳ (e dessa vez eu vou dar uma dica , elas são são **const**antes).

Agora nas versões mais recentes do JS temos uma nova e mais versátil versão de strings. Elas são principalmente utilizadas em **outputs** de variáveis, como o ... Você se lembra ? 🧠 Sim, nosso querido `alert()`. Você pode utilizá-las assim:\
`let nome = "Joãozinho"`\
`` alert(`O ${nome} não vai na aula hoje`) ``\
Ou seja, a variável que estiver depois do cifrão e entre as chaves `${}` será mostrada, e se você acha que pode usar um número no meio das chaves, você está redondamente CERTO. Tente:\
`let centena = 100` (observe que não temos aspas para declarar números)\
`` alert(`Uma centena tem o valor numérico de ${centena}`) ``

# Boolean

Esse tipo só tem dois valores: verdadeiro ou falso. Ou melhor `true` ou `false` em JS. Uma analogia 🙄 , eu sei que você está farto de analogias, é de um interruptor de luz. Ele só tem dois estados: ou está **desligado** ou **ligado**. Uma frase do tipo "A mulher está grávida 🤰 " ou é verdadeira ou falsa, não há um meio termo. Vamos nos adiantar um pouco no conteúdo e mostrar um operador, mas não conte para ninguém 🤫 . Se você colocar:\
`alert(0 > 1)`\
Você vai achar o valor `false`, que é do tipo booleano no seu console. Agora inverta o operador 🤷..Em vez de maior coloque menor como:\
`alert(0 < 1)`\
E o resultado vai ser `true`, porque 0 evidentemente é menor que 1. Agora para o que eu vou usar uma variável booleana nos meus programas, você deve estar se perguntando?
Bom, há vários motivos. e nós vamos listar alguns:

- Para utilizar como **flags** em seus programas. Flags são variáveis que sinalizam um comportamento, que pode ser falso/verdadeiro. Mas qualquer lógica binária pode ser adaptada para utilizar uma variável booleana. Pensando em um jogo, poderiámos usar uma variável booleana para simbolizar o estado vivo/morto de um personagem. `true` seria equivalente a vivo 😇 e `false` equivalente a morto 👻 . Dependendo do valor dessa variável o seu jogo poderia adotar um comportamento diferente.
- Para aplicar lógicas mais complexas em seus programas (como utilizar **OU**, **E**, **NOT** . Em breve ⏳ ) , e entender o resultado de todas essas operações lógicas.
- Para todas as partes no seu programa que não sejam números e textos e envolvam lógica de programação garantimos que uma variável booleana dará conta, desde que seu algoritmo esteja bem estruturado.
