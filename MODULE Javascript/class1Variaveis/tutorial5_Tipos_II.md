Estes foram os tipos mais comuns que você vai utilizar na sua vida inicial de programador JS, mas existem mais tipos (calma, não nos xingue 🤗, você não queria ser um programador JS?). Tome um 🧉 ou um ☕ e vamos seguir desbravando os tipos, em ordem de importância.

# Objects

Sem colocar os bois na frente da carroça, podemos dizer que um objeto em JS é um tipo de dado mais complexo que um **Number**, uma **String** ou um **Boolean** 🧠. Os tipos mencionados antes são conhecidos como **primitivos** e um Objeto é um tipo que faz uma referência. Mas na prática é que você pode designar propriedades para o seus objetos, de maneira que seus dados fiquem mais **organizados**. Para declarar um objeto podemos declarar um objeto vazio, ou um objeto e já preenchê-lo com atributos.
Declarando ele vazio:\
`let carro = {}` (Observe as chaves)\
E o preenchendo com propriedades\
`carro.cor = "amarelo"`\
`carro.velocidade = 250`\
`` alert(`Meu Camaro ${carro.cor} atinge a velocidade total de ${carro.velocidade}.`)``\
Ou declarar um já com as propriedades:

```
let carro2 = {
    cor : "vermelha",
    velocidade : 335
}
alert(`Minha Ferrari ${carro2.cor} atinge a velocidade total de ${carro2.velocidade}.`)
```

Em breve você verá mais sobre Objetos (o quê? Você está esperando o emoji da ampulheta que parece uma maçã? Ou você está de saco cheio com "Em breve isso", "Em breve aquilo"?). Em breve você saberá o porquê da paciência na programação ⏳😜 . Não , a verdade é que o seu, o meu, o cérebro de todo o ser humano tem um limite de aquisição e de relacionamento de informação. Então achamos melhor dar a você uma ideia do que será o material das próximas aulas do que despejar informação que talvez você não consiga assimilar no momento. OK?

# Undefined

Esse tipo é o pesadelo da maioria dos programadores JS . Ele quer dizer que a sua variável não possui um valor atrubuído. Com programas mais simples ele pode acontecer se você declarar a sua variável utilizando a palavra-chave `let` e se referindo a uma variável que não possui ainda um valor atribuído. Digamos que você cometa um erro de digitação no uso de sua variável. Por exemplo:\
`let nome = "meuNome"`\
`alert(NOME)`(Veja que você está se referindo a uma variável inexistente)\
Você verá que ocorrerá um erro no seu console `NOME is not deffined`. E se você fizer:\
`alert(typeof NOME)`\
ou\
`alert(typeof qualquerVariavelSemValor)`\
Você terá como retorno no seu console `undefined`. Isso ocorre também se você só declarar uma variável e não atribuir um valor a ela. Como:\
`let idade`(Repare que você declarou e não atribui nenhum valor)\
`alert(idade)`\
Essas são as maneiras mais comuns de se encontrar com o valor `undefined`, mas JS tem um estranho jeito de frustar suas expectativas como um programador iniciante. Se você achar essa mensagem `Uncaught ReferenceError: NomeDaSuaVariavel is not defined` ou achar o tipo `undefined`, mantenha a calma 🤞 e revise as atribuições de suas variáveis.

# Null

Esse tipo tem seus usos, relacionando-se principalmente como o tipo **Object**. O tipo `null` quer dizer que um objeto ou outra variável de referência perdeu sua referência. Ela é um jeito melhor de dizer que uma variável é **vazia**, **nada** ou **valor desconhecido** do que `undefined`. Ela serve para tipos de referência. Temos tipos primitivos e tipos de referência em JS.

Nesse momento é interessante você saber que os tipos **primitivos** são os tipos mais básicos em JS e são:

- Numbers
- Strings
- Booleans
- BigInts (um tipo introduzido recentemente para permitir números **MUITO GRANDES** . Pense no poder do Goku no final de Super Dragon Ball)
- Null
- Undefined
- Symbol (um tipo específico utilizado para identificar Objetos)

E os tipos que não são primitivos 🗿 ou tipos "complexos" 🧬 (não chame-os de complexados, eles são sensíveis quanto a isso 🤫 ):

- Objects
- Functions (funções que são como mini-programas reutilizáveis ⏳ )
