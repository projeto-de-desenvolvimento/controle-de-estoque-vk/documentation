Há mais algumas coisas que você precisa saber antes de decolar 🛫 como um programador JS. Você já sabe declarar variáveis e ver seus valores no console com a instrução `alert(variavel)` 🧠 . Note que o nome da variável declarada não é `variável`. Tente fazer isso no console do browser:\
`let variável = 10`\
E estranhamente isso funciona. 😱
Até algumas edições do JS atrás acentos gráficos no nome de variáveis, tais como `^~ç´` eram **proibidos**. Mas saiba que essa modificação é bem recente então encorajamos você a **SÓ USAR LETRAS E NÚMEROS NOS SEUS NOMES SEM ACENTOS GRÁFICOS**, com a exceção dos caracteres `$` (_dollar_ ou "cifrão") e `_` (_underline_ ou "sublinhado"). Mais algumas **regras** se aplicam . Calma, eu sei que regras são chatas, mas se queixe ao inventor do Javascript que as definiu 😏 . Elas são:

1. Não **começarás** o nome da tua variável com um número. Então o nome `5livros` é **ILEGAL** e não batizarás variável assim;
2. Poderás utilizar de **letras**, **números** (não no começo, se lembre da regra inicial ☝️), os caracteres **$** e **\_** para batizar vossas variáveis;
3. Não usarás espaços no nome das tuas variáveis. **NÃO FAÇA** `let nome do personagem`
4. Não usarás palavras reservadas da linguagem, tais como **let**, **var**, **function**, **new**, **return**.

Isso ficou um tanto dogmático e bíblico, mas era só para fixar melhor na sua cabeça e esperamos não ter ofendido nenhum religião 🙏 . Então **É PERMITIDO**:\
`let _limite`\
`let VidaMinimaPersonagem`\
`let caixa10dolares`\
`let dolar$euroCotacao`\
`let __sublinhados` (dois ou mais sublinhados são permitidos no início)\
`let $$dinheiros` (mesma coisa com o caractere de cifrão)

**NÃO FAÇAS**:\
`let maçãs` (embora as novas versões do JS permitam acentos gráficos, isso não é recomendado)\
`let 100reais` (não se pode começar um nome com um número)\
`let @email` (outros caracteres diferentes de `$` e `_` não funcionam)

E quanto a **maiúscúlas e minúsculas**? Faz diferença escrever `MeuPersonagem` e `meupersonagem`? A resposta é um sonoro 📣 **SIM**. Em JS e na grande maioria das linguagens faz muita diferença. Você está declarando duas variáveis diferentes se usar o **mesmo** nome só mudando entre **maiúsculas e minúsculas**. Chamamos sistemas que dão essa diferença de tratamento às palavras com essa diferenciação de **CASE SENSITIVE**. Então fique atento nos seus programas de que maneira você declarou suas variáveis.

Uma maneira boa de nomear suas variáveis se chama **camelcase** 🐫 e o nome vem das corcovas de um camelo mesmo. você começa com uma palavra com letras minúsculas e depois a próxima palavra terá só a primeira letra maíuscula e o resto minúsculo, e assim por diante. Assim:\
`let minhaPrimeiraVariavel`\
`let vidasJogadorUm`\
`let tituloFilmesAlugados`\
`let mensagemFinalJogo`\
Todos são bons nomes de variáveis que utilizam o sistema **camelcase**.
