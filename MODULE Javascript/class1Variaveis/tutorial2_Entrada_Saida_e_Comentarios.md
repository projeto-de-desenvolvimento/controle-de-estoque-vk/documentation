Legal, você já sabe como declarar uma **variável** em JS, como atribuir um **valor** a ela e como ver o tipo dela. Você se lembra da palavra-chave `typeof` 🧠 ?. Agora eu sei que você já quer utilizar suas variáveis em jogos super divertidos, como por exemplo o número de vidas que seu personagem tem: Poderíamos declarar no console do browser: `let vidas`. Se ele começa com 5 vidas podemos iniciar com o valor 5, então `vidas = 5` (note que 5 está sem aspas em sua volta. Isso vai ser importante no futuro ⏳ ). Se o seu personagem morreu 2 vezes, e o seu jogo alterou o valor de `vidas` e seu personagem não pegou nenhum 🍄 verde, você pode inspecionar o valor de uma variável no console do browser com a seguinte instrução `alert(vidas)`. O valor a ser impresso no console seria 3 no caso, a menos que estejamos em uma matemática estranha e 5 - 3 não seja mais igual a 2 😜 .

Ou saindo da ideia de jogo. Faça:\
`let altura = 1.65 `\
`alert("Eu tenho " + altura + " de altura")`

Atente-se que você deve utilizar ponto para separar a parte inteira da fracionária, e isso ocorre na maioria das linguagens de programação, porque adotam o sistema inglês de numeração 💂 . Sinta-se à vontade de colocar a altura que você possuir no valor da variável. A última coisa que você deve ter notado são as aspas e o texto adicionado dentro do parênteses. Estamos intercalando texto com o valor da sua variável com mais algum texto com o operador `+` (também em breve ⏳ ). O ponto é que você deve usar aspas simples ou duplas quando inserir texto entre os **parênteses** de uma instrução `alert`. Poderiámos ter escrito com aspas simples.\
`alert('Eu tenho ' + altura + ' de altura')` Com o mesmo efeito.

Há uma maneira, digamos, menos espalhafatosa, de realizar a sáida de suas variáveis. Ela é feita com a instrução `console.log()`, de forma similar ao alert. A diferença é que uma janela não pula na sua cara, e você pode usar `,` ao invés de `+` quando tiver mais de um dado de saída.\
`console.log("Eu tenho ", altura ," de altura")`Repare nas vírgulas

Até aí tudo bem, você consegue vazer os dados saírem do computador 💻 ▶🎲 . Agora como realizar a entrada de dados em JS? Afinal um programa não serve para muita coisa sem entrada de dados 💻 ◀🎲 pelo usuário. É para isso que serve a instrução `prompt()`. Você pode usá-la colocando uma pergunta dentro dos parênteses, com aspas simples ou duplas, e armazenar o que o usuário digitou logo em seguida em uma variável. Por exemplo:
`let gameFavorito = prompt("Qual o seu jogo favorito ever?")`\
`alert("seu jogo favorito é " + gameFavorito)`
Você pode colocar um valor padrão no prompt logo após a frase da pergunta. Assim:\
`let curso = prompt("Qual o seu curso?", "ADS")`\
`alert("O seu curso é " + curso + " mesmo? Legal.")`

Outra maneira de interação com o usuário é o `confirm()`. Você coloca uma pergunta entre os parênteses e a janela modal irá perguntar "OK" ou "Cancel". O resultado poderá alterar o fluxo do seu programa, quando vermos **condicionais** ⏳ . Assim:\
`confirm("O sentido da vida é 42?")`

Mais uma coisa, por favor não durma ainda ⏰😴? Precisamos falar de comentários. Não o que as pessoas comentam na rua diariamente, isso são fofocas. Comentários na programação servem para que você explique o raciocínio do seu algoritmo ou programa, e ajudam na manutenção do mesmo. Eles não interferem em nada na execução do programa. Você pode comentar em JS de duas maneiras:

- Com `//` (duas barras) para comentários de uma linha;
- Com `/* */` (barra, asterisco e depois asterisco, barra para fechar o comentário) para comentários de múltiplas linhas.

Na prática você pode fazer assim

`//essa variável irá armazenar o XP do personagem`\
`let xpPersonagemPrincipal = 100`

ou

`/* Esse jogo vai ser um RPG de turnos`\
`O jogador poderá ter 5 personagens ao mesmo tempo`\
`O personagem principal inicia com 100 de XP`\
`Toda a lógica do jogo virá a seguir */`

No começo pode ser um pouco trabalhoso comentar 🏋 , mas com a prática você verá que seus programas ficarão muito mais legíveis e de fácil manutenção 🔧🚗.
