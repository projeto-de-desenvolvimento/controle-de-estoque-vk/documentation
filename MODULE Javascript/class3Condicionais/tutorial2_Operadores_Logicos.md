Para essa aula recomendamos que você esteja com o raciocínio afiado. Jogue uma palavra-cruzada, um sudoku, até um Candy Crush vale 😉 . Dentro do `if` ou do `if else` da nossa aula anterior nós verificamos algumas condições, se um número ou um texto é igual a outro número ou texto, se um número é maior ou menor do que outro.

Até aí OK, nada demais. Mas suponhamos que além no nosso condicional verificar uma só situação não basta. Nós queremos testar além dos comparadores básicos, ou duas ou mais condições **ao mesmo tempo** no mesmo `if`. Aqui entram os operadores lógicos `||` "Or", `&&` "And" e `!` "Not" ("ou", "e", "não" respectivamente). Como eles funcionam 🙇 ? Calma, eles são bem intuitivos.

O de negação simplesmente nega um valor Boolean. O que é `true` vira `false`, e o que era `false` vira `true`. Ou V vira F, e F vira V. Ou se p é uma proposição do tipo : "Sócrates é um jogador de futebol". A negação é: "Sócrates não é um jogador de futebol" ou "Não é verdade que Sócrates é um jogador de futebol".
| p | !p |
|---|----|
| V | F |
| F | V |
"E para eu aplicar nos meu jog..programas!!! Que utilidade tem isso ?": 🤬 . Vamos mostrar com exemplos. p vai ser "o número é 42", e q vai ser "o planeta é a Terra".

```
let num = 42
let planeta = "Terra"
//no primeiro caso da tabela de negação temos p, "o número é igual a 42"
//então
let numIgual42 = (num === 42) //vai começar como true
//se aplicarmos o operador
numIgual42 = !numIgual42 //agora ele é false, mas perceba que num continua tendo o valor 42
```

Com os outros operadores a importãncia do Not vai ficar mais clara. Prometemos 🤞 . O operador `&&`, And só retorna verdadeiro se todas as comparações forem verdadeiras; no nosso caso quando ambos p **e** q forem verdadeiros. Quando "O número é 42" **E** "O planete é a Terra". Sinto que você quer uma tabela. Você é um/uma maniáco/a por tabelas e gráficos do Excel 📊 . Posso ver nos seus olhos. Essa é a tabela do `&&`:
| p | q | p && q |
|---|---|--------|
| V | V | V |
| V | F | F |
| F | V | F |
| F | F | F |

Agora já temos um operador a mais para brincar. Então:

```
if(num === 42 && planeta === "Terra") { ...}
if(num < 42 && planeta === "Marte") {...}
if(planeta !== "Terra" && num === 42) {..}
if(planeta !== "Vênus" && planeta !== "Júpiter") {...}
if(num <= 40 && num >= 42) {...}
```

Olhe a tabela verdade acima e tente descobrir em que blocos de `if` nosso programa entraria. É um bom exercício de lógica. E por último temos o operador `||`, Or e ele retorna verdadeiro quando **ao menos uma** condição é verdadeira; no caso de p **ou** q forem verdadeiros, ou um ou outro, tanto faz. Quando "O número é 42" **OU** "O planeta é a Terra". Nem vou perguntar, você já sabe o que é, seu/sua maniáco/a por números.
| p | q | p || q |
|---|---|--------|
| V | V | V |
| V | F | V |
| F | V | V |
| F | F | F |
