Agora seu treinamento inicial está completo, arquétipo de herói 🧙 . Você já está pronto para programar jog...softwares e apps mais complexos. Pegue seu sabre de luz, treine arduamente (revise o material das últimas aulas. Estamos tentando dar um ar de fantasia, mas estamos falhando miseravelmente 😔 ) e se prepare para o desafio que o encontra no mundo perigoso...OK 😞 . Vamos parar com isso. O conteúdo desta aula trata de **condicionais**, uma maneira de você realizar instruções diferentes com base em condições diferentes. Isso permite a você alterar o fluxo de execução do seu programa, e é uma das bases dos algoritmos (as outras são **repetições** e **funções** ⏳ ).
Para estruturar um condicional a primeira palavra chave que você precisa saber é `if` ou "se" em inglês. A sintaxe (forma de escrever de uma maneira que o computador entenda) correta do `if` é feita assim:\
`if (condição) instrução a ser feita`\
Ou com um exemplo prático:\
`let idade = Number(prompt("Entre com a sua idade"))`\
`if (idade >= 18) console.log("Você é maior de idade.")`
E a mensagem `Você é maior de idade` aparecerá no janela do seu browser se você tiver 19 ou mais anos (não vale enganar hem).
Se o número for menor que 18 nada acontece. Por quê 🤷 ? Pela razão de você ter usado um condicional `if`, e a instrução dentro do bloco `if` (vamos explicar isso logo adiante) só ocorre se a condição é verdadeira, ou seja, quando a idade é maior ou igual a 18.

"E se eu quiser realizar mais do que uma instrução dentro do if ?". Isso é possível, e vamos apresentar mais um símbolo na sintaxe de JS (e de várias linguagens), o `{ }`, chaves em português, ou braces em inglês 💂 . Garantimos que você vai usar muito esse símbolo nos seu programas. Sem adiantar muito o contéudo, as `{ }` chaves delimitam um **escopo** em que só o que você colocou entre as chaves é executado, dentro desse escopo. Ficou complicado? Com alguns exemplos a gente descomplica:

```
let ano = 2001
if (ano == 2001) {
    console.log("Nesse ano começou o século XXI")
    console.log("O filme " + ano + " é uma ótima ficção científica")
    console.log("Nesse ano ocorreram os atentados de 7 de Setembro")
}
console.log("Esse texto vai ser exibido independente da condição ser satisfeita porque está fora do bloco if")
```

Ficou um pouco mais claro 🔆 agora ? "E se a condição não for satisfeita"? Bom, no caso, o que está dentro dos parênteses retorna `false`, e todo o bloco dentro do `if` não será executado.

```
let anoPrompt = prompt("Entre com um ano. Pode ser o ano atual ou o do seu nascimento.")
if (anoPrompt == 2001) {
    console.log("Nesse ano começou o século XXI")
    console.log("O filme " + ano + " é uma ótima ficção científica")
    console.log("Nesse ano ocorreram os atentados de 7 de Setembro")
}
console.log("A condição do if não foi satisfeita, então tudo o que está entre chaves não será exibido")
```

OK. É bastante informação. Pense um pouco no que você acabou de ver 🤔 .Agora, uma dúvida deve ter surgido no último exemplo. o JS tem alguma maneira de executar o código quando a condição não é alcançada? A resposta é sim e você usará a palavra chave `else`, ou senão em português. Ela executa tudo o que não foi executado **imediatamente** depois do `if` (o que é `false` para o condicional). Então:

```
let marioEstaGrande = false
let vidasMario = 3
/*marioEstaGrande quer dizer que nosso Mário pegou um cogumelo vermelho e está em formato maior (pode ser atingido duas vezes). Mas nesse exemplo o nosso Mário não pegou nenhum cogumelo e está pequeno.
Digamos que se passa um tempo e em alguma situação do jogo o mário é atingido por um inimigo: em um jogo existe um loop (game loop) que fica constantemente rodando para verificar essas situações (eventos) */
if (marioEstaGrande){
    marioEstaGrande = false //agora ele está pequeno porque foi atingido
} else (merioEstaGrande) { //ele está pequeno porque não pegou nenhum cogumelo vermelho e é nessa situação que esse exemplo irá cair
    vidasMario-- //agora será 2
}
//e o ciclo retorna para a próxima vida do Mário
```

Mais um exemplo? Vamos lá:

```
let meuPokemonFavorito = "Charmander"
let seuPokemonFavorito = prompt("Qual o seu Pokemon favorito?")
if (meuPokemonFavorito === seuPokemonFavorito) {
    console.log("Nossos Pokemons favoritos são iguais!!!")
} else {
    console.log("Nossos Pokemons favoritos são diferentes.")
}
```

Você notou que utilizamos o operador `===` 🧠 ? É bom você se acostumar a utilizar esse operador ao invés do `==` para evitar futuras dores de cabeça 🤕 . Temos tamém a palavra-chave `else if`, senão se. E ela é assim mesmo, com um espaço no meio (um dos raros casos na sintaxe de JS). Você a usará para encadear várias sequências de condicionais que não foram satisfeitas no `if` inicial. Assim como o `if`, ela requer uma condição em parênteses.
Um exemplo:

```
let anoEntrada = prompt("Em que ano o Javascript foi criado?");
if (anoEntrada < 1995) {
    alert( "Muito cedo...");
} else if (year > 1995) {
    alert( "Muito tarde");
} else {
    alert("Acertou. Cacildis!");
}
```

E você pode colocar vários `if else` após um `if`. Isso vai depender da complexidade da **lógica** do seu programa. Uma outra coisa importante que você deve ter notado: dentro de um bloco `{ }` o texto está recuado um pouco. Isso se chama **IDENTAÇÃO** e é uma ótima prática de programação. Hoje em dia a maioria dos editores de código faz isso automaticamente, mas no tempo antigo 🧙 a identação era feita com a tecla TAB, ou usando espaços (geralmente 4) — e guerras ferrenhas eram travadas entre os programadores 🧑‍💻 ⚔ para decidir qual método era o melhor (evidentemente não se chegou a nenhuma conclusão e os programadores voltaram a jogar multiplayer para se chegar a um armistício e terminarem as guerras, que se tornaram virtuais).
