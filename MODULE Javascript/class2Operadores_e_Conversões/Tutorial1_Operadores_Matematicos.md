Você já deve ter visto estes operadores na sua vida certamente 👶 , mas não no contexto da programação. Eles funcionam de maneira muito parecida com o que você viu nas suas aulas de matemática ou como eles funcionam em uma calculadora. Faça:
`let a = 10`\
`let b = 5`\
`alert(a + b)`

E espero que 15 tenha aparecido no janela do seu browser. Até aí tudo normal. Temos também `- * /`\
Faça:
`alert(a - b)`\
`alert(a * b)`\
`alert(a / b)`\
E as respostas são 5, 50 e 2. "Essas foram fáceis, quero ver fazer 134.534 \* 45.667 / 73.856 !!!" 🤬 . Não se esqueça que um computador é uma máquina muito boa com números. Essa operação é feita em menos de um milésimo de segundo 😲 . Cabe a você como programador\ra traduzir fórmulas e conceitos matemáticos para uma linguagem de programação. E a propósito, a resposta da última pergunta é 83.185,715 aproximadamente.

Além desses operadores que você já deve ter visto, temos o operador **unário** `-` que troca de sinal um número (um número positivo vira negativo, por exemplo). Faça:
`let x = 42`\
`x = -x`\
`alert(x)` E o valor de x mudou de 42 para -42.
Ainda temos dois operadores que você não deve ter visto na escolinha, o `%` e o `**`. O primeiro retorna o resto de uma divisão e o segundo eleva um número a uma potência 🤯 . Calma, vamos mostrar alguns exemplos. Faça:\
`alert(5 % 2)` 5 / 2 é 2 com resto 1\
`alert(42 % 5)` 42 / 5 é 8 com resto 2\
`alert(2 ** 2)` 2² é 4\
`alert(3 ** 3)` 3³ é 27\
Legal? Mas a coisa vai ficar mais divertida 🥱 . Eu juro que vai. Matemática é divertida (se esqueça da sua professora chata que pegava no seu pé quando você dormia na aula de logarítmos). Não? bom, eu tentei.
Digamos que você queira usar uma variável para uma operação matemática e conservar o resultado na mesma variável. algo assim:
`let n = 2`\
`n = n + 5`\
Há um atalho para fazer isso em apenas uma linha. Dessa forma:\
`n += 5`\
E das duas maneiras n será 7. Isso poupa um pouco de escrita de código, e quando você se acostumar ficará até mais legível. Todas as outras operações `- * / % **` podem usar dessa forma de atalho. Como desafio tente para os outros operadores no seu terminal, com o outro número sendo 5 (lembre-se de reatribuir o valor de n para 2 se você as for fazer em sequência). As respostas vão ser 2, -3, 0,4 , 2 e 32 respectivamente.

Temos um outro atalho matreiro 😼 que JS nos dá. Se você quiser incrementar uma variável em 1, você pode fazer:\
`let i = 1`\
`i = i + 1`\
Ou da maneira que você acabou de aprender\
`i += 1`\
Mas há uma outra maneira ainda mais elegante\
`i++`\
Todas vão incrementar 1 em uma unidade. Que número é 1 + 1 😒 ? Mas o principal é que sempre que você querer incrementar em 1 a sua variável você pode fazer `variavel++`. Isso é muito útil em **contadores**, que são variáveis que contam 😠 quantas vezes determinada situação aconteceu no seu programa. Há uma diferença entre `variavel++` e `++variavel` (++ antes do nome). Mas isso fica para depois ⏳ .
