Você aprendeu os operadores matemáticos comuns como `+ - * /`. E até alguns mais avançados, como `%` e `**` : você se lembra do que eles fazem?
Mas e se o seu programa (a gente sabe que será um jogo 😉 ) envolver conceitos de física avançada 🚁 . Como você irá simular tudo sem que um desastre aconteça 🎇🪂 ? Talvez seja bom você dar uma olhada nas **funções matemáticas** que JS nos dá para fazer uma física top (claro e depois fazer um mestrado e um doutorado em Física, olha acho que é melhor usar um engine pronta...Não! Você consegue! Vamos lá 👩‍🔬 ).

Nós conseguimos usar algumas funções mais avançadas utilizando a classe **Math**. Você chama essa classe e depois uma função dessa classe, por exemplo para encontrar a raiz quadrada de um número `Math.sqrt(9)` e temos 3 como resposta. Vamos ver as funções mais úteis:

`Math.abs(num)` Retorna o valor absoluto de um número, ou seja, se for negativo, ele será convertido para positivo. Se ele já for positivo se manterá igual. Faça:\
`Math.abs(-42)` 42, o sentido da vida. Captou a referência? OK. Vamos parar de usar referências ao "Guia do Mochileiro das Galáxias". Nem é um livro tão legal assim.

`Math.ceil(num)` Arredonda um número com vírgula para cima. Funciona melhor dando um exemplo. Faça:\
`Math.ceil(4.2)` 5. O que? Quebramos a nossa promessa? Você está enxergando coisas.

`Math.floor(num)` Arredonda um número com vírgula para baixo para o próximo inteiro.\
`Math.floor(4.2)` 4\
`Math.floor(-4.2)` -5 🙄

`Math.pow(base, exp)` Retorna a base elevada ao expoente. Igual a qual operador que você já viu 🧠 ? Isso mesmo `**`.\
`Math.pow(4,2)` 16. Repare na vírgula, pois são dois argumentos.

`Math.round(num)` Arredonda um número com vírgula para o número inteiro mais próximo. A partir de .5 na parte fracionária ele arredonda para cima. Antes de .5, é arredondado para baixo.\
`Math.round(2.7)` 3\
`Math.round(2.4)` 2

`Math.trunc(num)` Retira a parte fracionária de um número.\
`Math.trunc(4.2)`4

`Math.sqrt(num)` Esse você já viu nesta aula. Ele retorna a raiz quadrada de um número.\
`Math.sqrt(36)` 6

`Math.min()` e `Math.max()` Retornam o valor menor e o valor maior de uma sequência de números, respectivamente.\
`Math.min(0, 150, 43, 20, -8, -200)` -200\
`Math.max(0, 150, 42, 20, -8, -200)` 150

`Math.random()` Esse é bem legal. Ele gera um número aleatório entre 0 e 1 (1 não incluso). Já vejo você pensando em aplicar em seus jogos. E obviamente a cada vez que você fizer o número vai ser diferente. Mas tem uns macetes para você usar essa função:
Você já sabe que o número retornado é um número fracionário que fica entre 0 e 1, e isso não parece muito útil. Mas se você multiplicar esse número por outro e aplicar a função `Math.floor()`, você terá um inteiro aleatório até esse número.
Para gerar um número aleatório de 0 até 9:\
`Math.floor(Math.random() * 10)`\
Para gerar um número aleatório de 0 até 10:\
`Math.floor(Math.random() * 11)`\
Para gerar um número aleatório de 0 até 100:\
`Math.floor(Math.random() * 101)`\
Para gerar um número aleatório de 1 até 100:\
`Math.floor(Math.random() * 100) + 1`Repare que estamos somando 1 no final então o resultado anterior que daria até 99, agora chega a 100. Acho que você pegou a ideia.

Ainda temos as funções `Math.log()` , `Math.log2()` , `Math.log10()` , `Math.sign()` , `Math.sin()` , `Math.cos()` , `Math.tan()`. E também as **constantes** `Math.PI` , `Math.E` , `Math.SQRT2` e outras para você brincar 👩‍🚀 e usar nos seus jog...programas.
