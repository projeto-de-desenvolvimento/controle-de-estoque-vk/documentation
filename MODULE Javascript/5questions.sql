# tutorial 1 exercícios 1

INSERT INTO `questions`(`statement`, `question`, `response`, `response_feedback`, `position`, `exercise_id`)
 VALUES ('Você não pode declarar uma variável em JavaScript utilizando a palavra-chave **var**', 
 'Esta afirmação é Verdadeira ou Falsa?', "F", 'Embora a palavra-chave var tenha caído em desuso, ela é perfeitamente utilizável para declarar variáveis. O seu uso é um pouco mais complexo do que **let** ', 1, 2);

INSERT INTO `questions`(`statement`, `question`, `response`, `response_feedback`, `position`, `exercise_id`)
 VALUES ('Uma variável nada mais é do que um espaço na memória RAM com um nome que se pretende usar de imediato ou no futuro', 
 'Esta afirmação é Verdadeira ou Falsa?', "V", 'Se você leu o tutorial bem essa questão foi facinho. Realmente uma variável é uma lugar na mamória RAM alocado para o seu programa', 2, 2);

INSERT INTO `questions`(`statement`, `question`, `response`, `response_feedback`, `position`, `exercise_id`)
 VALUES ('Javascript é uma linguagem fracamente tipada', 
 'Esta afirmação é Verdadeira ou Falsa?', "V", 'Javascript é fracamente tipada. Você pode atribuir um texto a uma variável e logo após um número; e depois um texto de novo. Isso faz a linguagem ser bastante flexível e atraente para iniciantes', 3, 2);

INSERT INTO `questions`(`statement`, `question`, `type`, `response`,`difficulty`, `alternative_1`, `alternative_2`, `alternative_3`, `alternative_4`,`position`, `exercise_id`)
 VALUES ('Você quer saber o tipo da variável `javaScript`. Com que instrução/palavra-chave você consegue isso?', 
 'Marque a alternativa correta', 'MULTIPLE', 'typeof javaScript', 3,'type javaScript', 'typeof javaScript','typeOf javaScript','type(javaScript)', 4, 2);

INSERT INTO `questions`(`statement`, `question`, `type`, `response`,`difficulty`,`position`, `exercise_id`) 
 VALUES ('Declaramos `let num = "123"`. Quando fizermos `alert(typeof num), o que será escrito na janela do browser?`', 'Escreva a resposta', 'STRING', 'String', 3, 5, 2);

INSERT INTO `questions`(`statement`, `question`, `type`, `response`,`difficulty`, `alternative_1`, `alternative_2`, `alternative_3`, `alternative_4`,`position`, `exercise_id`)
 VALUES ('Qual dessas linguagens também é fracamente tipada (ou realiza duck typing)?', 
 'Marque a alternativa correta', 'MULTIPLE', 'Python', 3,'C++', 'Java','C#','Python', 6, 2);

INSERT INTO `questions`(`statement`, `question`, `type`, `response`,`difficulty`,`position`, `exercise_id`) 
 VALUES ('Há uma linguagem de programação derivada do JavasCript, porém fortemente tipada. Você sabe qual?', 'Escreva a resposta', 'STRING', 'TypeScript', 2, 7, 2);

 # tutorial 2 exercicios 2

INSERT INTO `questions`(`statement`, `question`, `response`, `response_feedback`, `position`, `exercise_id`)
 VALUES ('Não há nada de errado com nesta declaração e atribuição `let valor = 3,45`.', 
 'Esta afirmação é Verdadeira ou Falsa?', "F", 'Vimos que você deve separar a parte inteira da parte fracionária com um "ponto", porque JS usa o sistema inglês de numeração', 1, 3);

 INSERT INTO `questions`(`statement`, `question`, `response`, `response_feedback`, `position`, `exercise_id`)
 VALUES ('Em JavaScript só se permite a saída de dados, pois ela está no paradigma OOP (Output Only Programming)', 
 'Esta afirmação é Verdadeira ou Falsa?', "F", 'JS permite sim a entrada de dados, tanto pelo usuário quanto por arquivos e outros métodos. OOP é uma sigla que significa outra coisa completamente diferente', 2, 3);

 INSERT INTO `questions`(`statement`, `question`, `response`, `response_feedback`, `position`, `exercise_id`)
 VALUES ('A função alert() permite a saída de dados em JS.', 
 'Esta afirmação é Verdadeira ou Falsa?', "V", 'Sim, o alert() é uma boa maneira, emobra um tanto espalhafatosa, de "imprimir" as suas variáveis e outros dados do seu programa.', 3, 3);