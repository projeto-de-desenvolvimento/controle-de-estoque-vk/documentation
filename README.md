## Este repositório está destinado a ser uma biblioteca dos documentos produzidos na Tese de Conclusão de Curso do Curso de Análise e Desenvolvimento de Sistema do Senac semestres Ago - Dez 2020, e Mar - Julho 2021

<h1>Bem vindo ao Back-End do projeto Conig Plataforma de Aprendizagem</h1>

Para acessar o nosso site basta clicar [aqui](https:\\app.conig.info) ou digitar no seu navegador <strong>app.conig.info</strong>!
Alguns usuários de teste:

Email: alunoteste@gmail.com <br>
Senha: conig123

Email: alunoteste2@gmail.com <br>
Senha: conig123
<h1>Onde eu encontro o WebApp?</h1>

Você consegue encontrar o WebApp [clicando aqui!](https://gitlab.com/projeto-de-desenvolvimento/controle-de-estoque-vk/schoolbooks-mobile)

<h1>Onde eu encontro o Backend?</h1>

Você consegue encontrar o Backend [clicando aqui!](https://gitlab.com/projeto-de-desenvolvimento/controle-de-estoque-vk/paartiucodar-back)
